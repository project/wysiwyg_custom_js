/**
 * @file
 * CKEditor plugin to inject custom JavaScript into the WYSIWYG iframe.
 */

CKEDITOR.plugins.add('wysiwyg_custom_js', {
  init : function (editor) {
    /**
     * Add JavaScript to the WYSIWYG iframe whenever it is loaded.
     */
    editor.on('mode', function(event) {
      if (event.editor.mode == 'wysiwyg') {
        var iframe = getIframe(event.editor);
        // Inject the JavaScript files and settings into the WYSIWYG iframe.
        // The list of JavaScript files may have duplicates (for example, due
        // to multiple WYSIWYG-enabled text formats on the same page) so ensure
        // it is unique first, since the same file should never be added twice.
        var javascript_files = [];
        for (var i = 0; i < Drupal.settings.wysiwyg_custom_js.files.length; i++) {
          if (javascript_files.indexOf(Drupal.settings.wysiwyg_custom_js.files[i]) === -1) {
            javascript_files.push(Drupal.settings.wysiwyg_custom_js.files[i]);
          }
        }
        // After injecting, attach Drupal behaviors to the iframe also.
        injectIframeScriptsInOrder(iframe, javascript_files, Drupal.settings.wysiwyg_custom_js.settings, attachBehaviorsToIframe);
      }
    });

    /**
     * Reattach Drupal behaviors whenever anything in the WYSIWYG changes.
     */
    editor.on('change', function (event) {
      // Attaching behaviors here ensures that the iframe is always updated to
      // have the correct HTML based on whatever the user edits in the WYSIWYG.
      var iframe = getIframe(event.editor);
      attachBehaviorsToIframe(iframe);
    });

    /**
     * Detach Drupal behaviors when retrieving raw HTML from the WYSIWYG.
     */
    editor.dataProcessor.toDataFormat = CKEDITOR.tools.override(editor.dataProcessor.toDataFormat, function(originalToDataFormat) {
      // Detaching behaviors here allows any JavaScript-based styling that was
      // added for the WYSIWYG to be removed before the raw content is saved.
      // This is done here rather than responding to the 'getData' event to
      // ensure behaviors are detached on the exact same HTML that exists in
      // the WYSIWYG (before CKEditor itself changes any HTML, or modules like
      // Media convert HTML tags back into tokens). This helps ensure that the
      // JavaScript is able to run correctly on the HTML it expects to run on.
      // See also Drupal.wysiwyg.editor.attach.ckeditor.pluginsLoaded() where
      // some of this code is borrowed from.
      return function(data, fixForBody) {
        var iframe = getIframe(editor);
        // It is necessary to pass in the data to work with (rather than
        // modifying the iframe directly here) since it is possible for the
        // WYSIWYG content to be retrieved for "read-only" purposes even when
        // the iframe is not about to be destroyed (for example, the Autosave
        // module does this).
        data = detachBehaviorsFromIframeProvidedContent(iframe, data);
        // Once behaviors have been detached, call the original method to allow
        // CKEditor itself and other WYSIWYG plugins to modify the HTML further.
        data = originalToDataFormat.call(this, data, fixForBody);
        return data;
      };
    });

    /**
     * Detach Drupal behaviors when a CKEditor dialog is opened and populated.
     */
    CKEDITOR.dialog.prototype.setupContent = CKEDITOR.tools.override(CKEDITOR.dialog.prototype.setupContent, function (originalSetupContent) {
      // Detaching Drupal behaviors here ensures that any JavaScript-based
      // styling that was added for the WYSIWYG will not interfere with
      // CKEditor dialogs that are used to edit content within the WYSIWYG.
      // (For example, this is useful for image dialogs that need to edit a raw
      // <img> tag, but where the site's JavaScript-based captioning solution
      // may have moved or altered the <img> tag in the WYSIWYG in order to add
      // corresponding <figure> and <figcaption> tags. The <img> tag needs to
      // be restored to its former state in order for the dialog to edit it.)
      return function () {
        for (i = 0; i < arguments.length; i++) {
          // Find any DOM element that is being edited, and detach behaviors
          // from it.
          if (typeof arguments[i] === 'object' && arguments[i] instanceof CKEDITOR.dom.element) {
            var iframe = getIframe(editor);
            var data = arguments[i].$.outerHTML;
            var div = document.createElement('div');
            // Add a special attribute to the wrapper so detach methods can
            // determine that they are being called in the setupContent
            // context.
            var attributes = {'data-wysiwyg-custom-js-setup-content': 1}
            div.innerHTML = detachBehaviorsFromIframeProvidedContent(iframe, data, attributes);
            arguments[i] = new CKEDITOR.dom.element(div.firstChild);
          }
        }
        return originalSetupContent.apply(this, arguments);
      };
    });

    /**
     * Retrieves the WYSIWYG iframe from a CKEditor editor object.
     *
     * @return
     *   The DOM node corresponding to the WYSIWYG iframe associated with the
     *   provided editor.
     */
    var getIframe = function (editor_object) {
      return jQuery('#' + editor_object.id + '_contents iframe').get(0);
    };

    /**
     * Attaches Drupal behaviors to the contents of an iframe.
     *
     * @param iframe
     *   The DOM node corresponding to the iframe.
     */
    var attachBehaviorsToIframe = function (iframe) {
      // Only attach behaviors conditionally in case the iframe contents aren't
      // fully loaded yet.
      if (iframe && iframe.contentWindow && iframe.contentWindow.Drupal && iframe.contentWindow.Drupal.attachBehaviors && iframe.contentWindow.Drupal.settings) {
        iframe.contentWindow.Drupal.attachBehaviors(iframe.contentWindow.document, iframe.contentWindow.Drupal.settings);
      }
    };

    /**
     * Detaches Drupal behaviors from the provided contents of an iframe.
     *
     * @param iframe
     *   The DOM node corresponding to the iframe.
     * @param string data
     *   An HTML string representing the contents of the <body> tag that should
     *   have its behaviors detached within the iframe.
     * @param object attributes
     *   (Optional) An object whose keys are attribute names and values are
     *   attribute values. If set, the provided attributes will be added to the
     *   wrapper div that contains the provided content and that is passed to
     *   the Drupal detach implementations. This can be used to allow the
     *   detach code to determine what context it is being detached in.
     *
     * @return string
     *   An HTML string representing the new contents of the <body> tag, after
     *   all behaviors have been detached.
     */
    var detachBehaviorsFromIframeProvidedContent = function (iframe, data, attributes) {
      // Only detach behaviors conditionally in case the iframe contents aren't
      // fully loaded yet.
      if (iframe && iframe.contentWindow && iframe.contentWindow.Drupal && iframe.contentWindow.Drupal.detachBehaviors && iframe.contentWindow.Drupal.settings) {
        // Create a special wrapper element with the desired contents, and pass
        // that in to the detach function. Add an ID in case any custom
        // JavaScript needs to detect the wrapper's presence. Also add any
        // attributes that were requested by the calling code.
        var wrapper = document.createElement('div');
        wrapper.id = 'wysiwyg-custom-js-content-wrapper';
        if (attributes) {
          for (var name in attributes) {
            if (attributes.hasOwnProperty(name)) {
              wrapper.setAttribute(name, attributes[name]);
            }
          }
        }
        wrapper.innerHTML = data;
        iframe.contentWindow.Drupal.detachBehaviors(wrapper, iframe.contentWindow.Drupal.settings);
        return wrapper.innerHTML;
      }
      else {
        return data;
      }
    };

    /**
     * Injects JavaScript into an iframe in a specified order.
     *
     * This function injects JavaScript files and inline JavaScript into an
     * existing iframe while ensuring that the correct order is preserved so
     * that dependencies are met. After everything has been injected and
     * finished loading, a callback is triggered.
     *
     * @param iframe
     *   The DOM node corresponding to the iframe.
     * @param array script_files
     *   An array of filenames representing the JavaScript files to load, in
     *   the order in which they should be loaded.
     * @param string inline_script
     *   A string representing JavaScript code that should be executed inline.
     *   This code will run after all the above JavaScript files have been
     *   loaded. It is primarily intended for the code that populates
     *   Drupal.settings.
     * @param callback
     *   A callback function to run after all JavaScript has been added and
     *   finished loading. This function will be passed the iframe DOM node as
     *   the first and only parameter.
     */
    var injectIframeScriptsInOrder = function (iframe, script_files, inline_script, callback) {
      var iframe_document = iframe.contentWindow.document;

      // Clone the list of script files to prevent this function from modifying
      // the version that the calling code sees.
      var scripts = script_files.slice(0);

      // Create a script element for the first JavaScript file, then add an
      // onload handler to recursively trigger this function for the next file
      // once the first is finished loading. In the case of JavaScript which is
      // dynamically injected after the iframe has finished loading, this
      // ensures that the JavaScript files load in the correct order such that
      // any dependencies are met. Parts of this code are roughly based on the
      // example at http://stackoverflow.com/questions/16230886/trying-to-fire-onload-event-on-script-tag/16231055.
      var script_element = iframe_document.createElement('script');
      script_element.type = 'text/javascript';
      script_element.src = scripts.shift();
      script_element.onload = function () {
        if (scripts.length) {
          injectIframeScriptsInOrder(iframe, scripts, inline_script, callback);
        }
        else {
          // All the script files have finished loading, so add the inline
          // scripts afterwards.
          var inline_script_element = iframe_document.createElement('script');
          inline_script_element.type = 'text/javascript';
          inline_script_element.appendChild(iframe_document.createTextNode(inline_script));
          iframe_document.head.appendChild(inline_script_element);
          // Finally, execute the callback function.
          callback(iframe);
        }
     };
     iframe_document.head.appendChild(script_element);
    };
  }
});
