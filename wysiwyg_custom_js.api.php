<?php

/**
 * @file
 * Hooks provided by the WYSIWYG Custom JavaScript module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add to or alter the custom JavaScript injected into the WYSIWYG iframe.
 *
 * @param array $js_files
 *   An array whose keys are the names of JavaScript files that should be
 *   injected into the WYSIWYG iframe (in the order in which they should be
 *   injected) and whose values are JavaScript file options. Add elements to
 *   this array in order to add files to the WYSIWYG. The values are usually
 *   left empty but can be used (for example) to set 'version' in the case of a
 *   JavaScript library file, or 'cache' (to FALSE) in the case of a JavaScript
 *   file that should never be cached. See drupal_add_js().
 * @param array $js_settings
 *   A numerically-indexed array of Drupal JavaScript settings. Each element's
 *   keys indicate the name of the setting, and each element's value indicates
 *   the value of the setting. Add elements to this array in order to add
 *   Drupal settings to the WYSIWYG.
 * @param array $context
 *   An array of context information about the WYSIWYG. Currently this contains
 *   one value:
 *   - element: A Drupal render array representing the text format element that
 *     the WYSIWYG will be attached to.
 *
 * @todo This hook should have additional context passed in (in particular the
 *   WYSIWYG profile that is currently active), and the JavaScript files and
 *   settings should be segmented by WYSIWYG instance. Otherwise the exact same
 *   JavaScript is always forced to be injected into all WYSIWYGs on the same
 *   page.
 */
function hook_wysiwyg_custom_js_alter(&$js_files, &$js_settings, $context) {
  // Inject a JavaScript file.
  $filename = drupal_get_path('module', 'my_module') . '/js/mymodule.js';
  $js_files[$filename] = array();

  // Inject some JavaScript settings into Drupal.settings.some_setting and
  // Drupal.settings.my_module.my_setting, respectively.
  $js_settings[] = array('some_setting' => 'some value');
  $js_settings[] = array('my_module' => array('my_setting' => 'some value'));
}

/**
 * @} End of "addtogroup hooks".
 */
